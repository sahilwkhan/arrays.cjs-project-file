const array_object = require('./arrays.cjs');
elements = array_object.items;

const find_function = require('./find.cjs');

function cb(element) {
    return element == 5;
}

result = find_function(elements,cb);

console.log(result)
