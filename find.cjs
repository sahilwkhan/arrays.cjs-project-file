module.exports = find;

function find(elements, cb) {
    if (typeof elements != 'object' || typeof cb != 'function' ) {
        return [];
    }
    else {
        for (let index = 0; index < elements.length; index++){
            if (cb(elements[index])) {
                return elements[index];
            }
        }
        return undefined;
    }
}
