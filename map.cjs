module.exports = map;

function map(elements_list, cb) {
    const map_array = [];
    if (typeof elements_list != 'object' || typeof cb != 'function') {
        return [];
    } 


    for (let iterator = 0; iterator < elements_list.length; iterator++) {
        map_array.push(cb(elements_list[iterator], iterator, elements_list));
    }

    return map_array;
}