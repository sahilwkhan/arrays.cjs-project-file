module.exports = reduce;

function reduce(elements, cb,startingValue) {
    if (typeof elements != 'object' || typeof cb != 'function' ) {
        return [];
    }
    else {
        let start, accumulator;
        if (startingValue == undefined) {     
            accumulator = elements[0];
            start = 1;
        } else {
            start = 0;
            accumulator = startingValue;
        }
        for (let index = start; index < elements.length; index++){
            accumulator =  cb(accumulator,elements[index], index, elements,startingValue);
        }
        return accumulator;
    }
}
// console.log("elements["+index+"] =",elements[index], " and startingValue =",startingValue);
