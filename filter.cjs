module.exports = filter;

function filter(elements, cb) {
    if (typeof elements != 'object' || typeof cb != 'function' ) {
        return [];
    }
    else {
        const passed_test = []
        for (let index = 0; index < elements.length; index++){
            if (cb(elements[index],index,elements) === true) {
                passed_test.push(elements[index]);
            }
        }
        return passed_test;
    }
}
