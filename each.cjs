module.exports = each;

function each(elements, cb) {
    if (typeof elements != 'object' || typeof cb != 'function') {
        return [];
    }
    else {
        updated_elements = [];
        for (let index = 0; index < elements.length; index++) {
            cb(elements[index]);
        }  
    }
}