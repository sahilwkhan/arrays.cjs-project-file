module.exports = flatten;

const reduce = require('./reduce.cjs');


function flatten_function(array, depth) {
    
    for (let element = 0; element < array.length; element++) {    
        if (typeof array[element] == 'object') {
            
            if (depth > 0) {
                flatten_function(array[element], depth - 1 );
            }
            else {
                flat_array.push(array[element]);
            }
        }
        else {
            if (array[element] != undefined) {
                flat_array.push(array[element]);
            }
      
        }
    }
}

const flat_array = [];

function flatten(array, depth) {
    if (array == undefined || typeof array != 'object') {
        return [];
    }
    if (depth == undefined) {
        depth = 1;
    }
    
    flatten_function(array, depth);
    return flat_array;
}


