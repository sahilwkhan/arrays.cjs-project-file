const array_object = require('./arrays.cjs');
elements = array_object.items;

const reduce_function = require('./reduce.cjs');

function cb(accumulator,currentValue, index, array,startingValue) {
    return accumulator + currentValue;
}

result = reduce_function(elements, cb);

console.log(result);
